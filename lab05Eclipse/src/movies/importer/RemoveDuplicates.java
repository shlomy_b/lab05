package movies.importer;

import java.util.ArrayList;

public class RemoveDuplicates extends Processor{

	public RemoveDuplicates(String sourceDir, String outputDir) {
		super(sourceDir, outputDir, false);
	}
	
	public ArrayList<String> process(ArrayList<String> arr) {
		ArrayList<String>tempArr = new ArrayList<String>();
		
		for(int i = 0; i < arr.size(); i++) {
			if(!(tempArr.contains(arr.get(i)))) {
				tempArr.add(arr.get(i));
			}
		}
		return tempArr;
	}
}

