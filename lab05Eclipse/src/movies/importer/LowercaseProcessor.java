package movies.importer;
import java.util.*;

public class LowercaseProcessor extends Processor{
	
	public LowercaseProcessor(String sourceDir, String outputDir){
		super(sourceDir, outputDir, true);	
	}
	
	public ArrayList<String> process(ArrayList<String> arr) {
		ArrayList<String>asLower = new ArrayList<String>();
		
		for(int i = 0; i < arr.size(); i++) {
			asLower.add(arr.get(i).toLowerCase());
		}
		return asLower;
	}
}
